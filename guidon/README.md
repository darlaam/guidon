# guidon

[Documentation](https://docs.rs/guidon)  

*guidon* is a french word for handlebars.  
guidon is a small library which aims to speed up project initialization with templates.  

guidon has a command line interface [guic](https://crates.io/crates/guidon-cli)

```toml
[dependencies]
guidon = "0.3"
```

guidon performs templating based on [handlebars](https://handlebarsjs.com/) templating system.

## Usage
Files to be handles needs to have an `.hbs` extension. Folders and files names can be templatized too : `{{folder/subfolder}}`  
The entry point is the [Guidon](https://doc.rs/guidon/0.2.7/guidon/struct.Guidon.html) structure.

### Basic init

```rust, no_run
use guidon::Guidon;
use std::collections::HashMap;
use std::path::PathBuf;

let mut guidon = Guidon::new(PathBuf::from("path/to/template/dir"));
let mut vars = HashMap::new();
vars.insert("key1".to_string(), "value1".to_string());
guidon.variables(vars);
guidon.apply_template("path/to/destination").unwrap();
```
With this initialization:
* guidon will use the vars map to find substitution values.
* guidon will parse the directory `path/to/template/dir/template`
* the result will be written in `path/to/destination`

### TryNew init
guidon implements a TryNew trait to initialize from a dir or a git repo

#### TryNew from path
```rust, no_run
use guidon::{Guidon, TryNew};

let mut guidon = Guidon::try_new("path/to/template/dir").unwrap();
```
With this initialization:
* guidon will init the substitution variables from `path/to/template/dir/variables.toml`
* guidon will parse the directory `path/to/template/dir/template`

```rust, no_run
use guidon::{Guidon, TryNew};

let mut guidon = Guidon::try_new("path/to/template/dir/my_vars.toml").unwrap();
guidon.use_template_dir(false);
```
With this initialization:
* guidon will init the substitution variables from `path/to/template/dir/my_vars.toml`
* guidon will parse the directory `path/to/template/dir`

#### TryNew from CustomTemplate
Guidon can be provided with specific template file and directory to parse.
```rust, no_run
use guidon::{CustomTemplate, Guidon, TryNew};
use std::path::Path;
let custom = CustomTemplate {
     dir_path: Path::new("path/to/template/dir"),
    template_path: Path::new("path/to/my_vars.toml")
};
let mut guidon = Guidon::try_new(custom);
```

#### TryNew from git repo
```rust, no_run
use guidon::{{Guidon, TryNew, GitOptions}};

let git = GitOptions::builder()
    .repo("url/to/repo")
    .build()
    .unwrap();
let mut guidon = Guidon::try_new(git);
```
With this initialization
* guidon will clone the repo to a temporary directory
* guidon will init the substitutions variables from `tmp/dir/template.toml`
* when applying template, guidon will parse the directory `tmp/dir/template`

The git repo url could be in the form :
* `http[s]://[user[:password]]@uri/repo[.git]` where *user* and *password* must be part of the url
* `git@uri:repo.git` : the key used must be loaded in an *ssh-agent* instance

## Template variables
The configuration file is structured as follows :
```toml
# Key value pairs for template substitution
[variables]
test1 = "test 1"
test2 = "test 2"
```

## Features
The git initialization is a feature that can be deactivated.  
In Cargo.toml
```toml
[dependencies.guidon]
version = "0.2"
default-features = false
```

## Callbacks
guidon offers the possiblity to provide two callbacks :
* a variables callback to operate on the variables before applying the template
* a render callback, which is called if a missing value is detected while rendering the template.

These callbacks could be use for user interaction, or default behaviour.

### Variable callback
In this example, the callback will add `" cb"` to every value.
```rust, no_run
  use guidon::Guidon;
  use std::collections::HashMap;
  use std::path::PathBuf;
  let cb = |h: &mut HashMap<String, String>| {
          h.iter_mut().for_each(|(_, v)|  *v +=" cb");
  };
  let mut guidon = Guidon::new(PathBuf::from("template/path"));
  guidon.set_variables_callback(cb);
```
### Render callback
In this example, the callback will replace any missing value by the key concatenated with `"-cb"`
```rust, no_run
use guidon::Guidon;
use std::collections::HashMap;
use std::path::PathBuf;

// this callback will add `-cb` to the key as a value
let cb = |h: String| {
        let mut s = h.clone();
        s.push_str("-cb");
        s
 };
let mut guidon = Guidon::new(PathBuf::from("template/path"));
guidon.set_render_callback(cb);
```

## Templates
Templatized files must have the `.hbs` extension. When rendered, this extension will be 
deleted (eg. a `MyHelloWord.java.hbs` file will be rendered as a `MyHelloWorld.java` file).

Templatized values are declared with "mustaches" : {{myKey}}.
Values for these keys can be provided by :
* a HashMap<K,V> provided by calling `Guidon::variables`
* a `template.toml` file

### Helpers
Some helpers are provided : 
* `replace`. It's simply replace a string by another in the value.  
`Tell me: {{ replace my_var everybody world }}` with `my_var="Hello everybody !` will render as 
`Tell me: Hello world !`.
* `append`. Simply append a string to the value.
`Tell me: {{ append my_var "and everybody !" }}` with `my_var="Hello world"` will render as
`Tell me: Hello world and everybody !`.
* `prepend`. Prepend a string to the value.
`Tell me: {{ prepend my_var "Sure, " }}` with `my_var="hello world"` will render as `Tell me: Sure, hello world`.
* `up`: Uppercase the value
* `low`: Lowercase the value
* `encrypt`: Encrypt the data. The key must be provided
* `decrypt`: Decrypt the data

### File and Directory names
Filnames and directory can also be templatized.  
`Super-{{my_var}}-content.txt` will render as `Super-boring-content.txt` given `my_var="boring"`.
If the content of the file is templatized, we have `Super-{{my_var}}-content.txt.hbs`.

### template.toml structure
The template.toml structure is the following: 
```toml
[variables]
my_var="my value"
my_other_var="oh my, I need to find an idea…"
```

## Logs
guidon uses the log facade.



## Minimum rust version
The minimum rust version is `1.38.0`

# License

This project is licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](../LICENSE-APACHE) or
   http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](../LICENSE-MIT) or
   http://opensource.org/licenses/MIT)

at your option.  

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in guidon by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
