//! # Crypto module for guidon
//!
//!
use crate::errors::*;
use core::{char, str};
use crypto::buffer::{BufferResult, ReadBuffer, WriteBuffer};
use crypto::{aes, blockmodes, buffer};
use rand::distributions::{Distribution, Uniform};
use rand::{thread_rng, Rng};
use std::env;

const DEFAULT_IV: &str = "DEFAULTIV_GUIDON";

#[derive(Debug)]
struct ASCII;

impl Distribution<char> for ASCII {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> char {
        let range = Uniform::new(33, 127);
        loop {
            let n = range.sample(rng);
            if n != 34 && n != 39 {
                return unsafe { char::from_u32_unchecked(n) };
            }
        }
    }
}

/// Generate a random key of given length.
///
/// To use with gencrypt/gdecrypt prefer a lenght of 256
///
/// # Arguments
/// * `len`: length. The expected key length
///
/// # Example
/// ```rust
/// # use guidon::gen_key;
/// let key = gen_key(16);
/// println!("{}", key);
/// ```
///
#[allow(dead_code)]
pub fn gen_key(len: usize) -> String {
    let mut rng = thread_rng();
    let rstr: String = (&mut rng)
        .sample_iter(ASCII)
        .take(len)
        .map(char::from)
        .collect();
    rstr
}

fn parse_hex(hex_str: &str) -> Vec<u8> {
    let mut hex_bytes = hex_str
        .as_bytes()
        .iter()
        .filter_map(|b| match b {
            b'0'..=b'9' => Some(b - b'0'),
            b'a'..=b'f' => Some(b - b'a' + 10),
            b'A'..=b'F' => Some(b - b'A' + 10),
            _ => None,
        })
        .fuse();

    let mut bytes = Vec::new();
    while let (Some(h), Some(l)) = (hex_bytes.next(), hex_bytes.next()) {
        bytes.push(h << 4 | l)
    }
    bytes
}

/// Encrypt a string with the given key
///
/// The key can be provided by an environment variable `GUIDON_KEY` or by the key option.
/// As of now, the environment variable will be used if set, even if a key is provided as an
/// argument.
/// A initialization vector (IV) can also be provided by environment variable `GUIDON_IV`. If not
/// the default on will be used. The IV must be 16 chars.
///
/// # Arguments
/// * `data`: The message to encrypt
/// * `key`: An option of the key to use. The key length should be 256.
///
/// # Example
/// ```rust
/// # use guidon::{gen_key, gencrypt};
/// let key = gen_key(256);
/// let enc_msg = gencrypt("Hello world !", Some(&key)).unwrap();
/// println!("{}", enc_msg);
/// ```
///
pub fn gencrypt(data: &str, key: Option<&str>) -> Result<String> {
    let guidon_key = env::var("GUIDON_KEY")
        .map_or_else(
            |_e| {
                key.map(String::from)
                    .ok_or(|| GuidonError::from("No key provided"))
            },
            Ok,
        )
        .map_err(|_| GuidonError::from_string("Can't retrieve key !"))?;

    let guidon_iv = env::var("GUIDON_IV").unwrap_or_else(|_| DEFAULT_IV.to_string());

    let enc_data = encrypt(data.as_bytes(), guidon_key.as_bytes(), guidon_iv.as_bytes())?;
    let ret: String = enc_data.iter().map(|u| format!("{:02X?}", u)).collect();
    Ok(ret)
}

/// Decrypt a string with the given key
///
/// The key can be provided by an environment variable `GUIDON_KEY` or by the key option.
/// As of now, the environment variable will be used if set, even if a key is provided as an
/// argument.
/// A initialization vector (IV) can also be provided by environment variable `GUIDON_IV`. If not
/// the default on will be used. The IV must be 16 chars.
///
/// # Arguments
/// * `data`: The message to decrypt
/// * `key`: An option of the key to use. The key length should be 256.
///
/// # Example
/// ```rust
/// # use guidon::{gen_key, gencrypt, gdecrypt};
/// let key = gen_key(256);
/// let enc_msg = gencrypt("Hello world !", Some(&key)).unwrap();
/// let msg = gdecrypt(&enc_msg, Some(&key)).unwrap();
/// assert_eq!("Hello world !", msg);
/// ```
///
pub fn gdecrypt(data: &str, key: Option<&str>) -> Result<String> {
    let guidon_key = env::var("GUIDON_KEY")
        .map_or_else(
            |_e| {
                key.map(String::from)
                    .ok_or(|| GuidonError::from("No key provided"))
            },
            Ok,
        )
        .map_err(|_| GuidonError::from_string("Can't retrieve key !"))?;
    let guidon_iv = env::var("GUIDON_IV").unwrap_or_else(|_| DEFAULT_IV.to_string());

    let enc_data = parse_hex(data);

    let dec_data = decrypt(&enc_data[..], guidon_key.as_bytes(), guidon_iv.as_bytes())?;
    let ret = String::from_utf8(dec_data)?;
    Ok(ret)
}

fn encrypt(data: &[u8], key: &[u8], iv: &[u8]) -> Result<Vec<u8>> {
    let mut encryptor =
        aes::cbc_encryptor(aes::KeySize::KeySize256, key, iv, blockmodes::PkcsPadding);
    let mut final_result = Vec::<u8>::new();
    let mut read_buffer = buffer::RefReadBuffer::new(data);
    let mut buffer = [0; 4096];
    let mut write_buffer = buffer::RefWriteBuffer::new(&mut buffer);

    loop {
        let result = encryptor.encrypt(&mut read_buffer, &mut write_buffer, true)?;

        final_result.extend(
            write_buffer
                .take_read_buffer()
                .take_remaining()
                .iter()
                .copied(),
        );

        match result {
            BufferResult::BufferUnderflow => break,
            BufferResult::BufferOverflow => {}
        }
    }

    Ok(final_result)
}

fn decrypt(encrypted_data: &[u8], key: &[u8], iv: &[u8]) -> Result<Vec<u8>> {
    let mut decryptor =
        aes::cbc_decryptor(aes::KeySize::KeySize256, key, iv, blockmodes::PkcsPadding);

    let mut final_result = Vec::<u8>::new();
    let mut read_buffer = buffer::RefReadBuffer::new(encrypted_data);
    let mut buffer = [0; 4096];
    let mut write_buffer = buffer::RefWriteBuffer::new(&mut buffer);

    loop {
        let result = decryptor.decrypt(&mut read_buffer, &mut write_buffer, true)?;
        final_result.extend(
            write_buffer
                .take_read_buffer()
                .take_remaining()
                .iter()
                .copied(),
        );
        match result {
            BufferResult::BufferUnderflow => break,
            BufferResult::BufferOverflow => {}
        }
    }

    Ok(final_result)
}

#[cfg(test)]
mod tests {
    use crate::crypto::{decrypt, encrypt, gencrypt, gdecrypt, gen_key};
    use log::info;
    use log::LevelFilter;
    use std::sync::Once;

    static INIT: Once = Once::new();

    fn setup() {
        INIT.call_once(|| {
            let _ = env_logger::builder()
                .is_test(true)
                .filter_level(LevelFilter::Trace)
                .try_init();
        });
    }

    #[test]
    fn should_encrypt_decrypt() {
        setup();
        let message = "Hello World!";

        let key = gen_key(256);
        let iv = gen_key(16);

        let encrypted_data = encrypt(message.as_bytes(), &key.as_bytes(), &iv.as_bytes())
            .ok()
            .unwrap();
        let decrypted_data = decrypt(&encrypted_data[..], &key.as_bytes(), &iv.as_bytes())
            .ok()
            .unwrap();

        assert_eq!(message.as_bytes(), &decrypted_data[..]);
    }

    #[test]
    fn should_gencrypt_gdecrypt() {
        setup();
        let message = "Hello World!";

        let key = gen_key(256);
        info!("Key: {}", key);
        let encrypted_data = gencrypt(message, Some(&key)).ok().unwrap();
        info!("Encrypted data: {}", encrypted_data);
        let decrypted_data = gdecrypt(&encrypted_data, Some(&key)).ok().unwrap();
        assert_eq!(message, &decrypted_data);
    }

    #[test]
    fn test_gen_key() {
        let key = gen_key(256);

        assert_eq!(key.len(), 256);
    }
}
