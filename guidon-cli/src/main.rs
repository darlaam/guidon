// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use guidon::{gdecrypt, gen_key, gencrypt, GitOptions, Guidon, GuidonError, Result, TryNew, CustomTemplate};
use std::error::Error;

use gumdrop::Options;
use log::{error, LevelFilter};
use simple_logger::SimpleLogger;
use std::collections::BTreeMap;
use std::io::Write;
use std::str::FromStr;
use std::path::Path;

fn guic_template(opts: &TemplateOpts) -> Result<()> {
    let mut guidon = init_guidon(opts)?;
    match opts.mode {
        Mode::Strict => {
            guidon.use_strict_mode(true);
        }
        Mode::Lax => {
            guidon.use_strict_mode(false);
        }
        Mode::StrictAsk => {
            guidon.use_strict_mode(true);
            guidon.set_render_callback(render_callback);
        }
    }
    if opts.interactive {
        guidon.set_variables_callback(variables_callback)
    }
    guidon.apply_template(&opts.to_path)
}

fn main() {
    let opts: CliOpts = CliOpts::parse_args_default_or_exit();
    init_logs(opts.verbose);

    match opts.command.unwrap() {
        Command::Template(tplt) => {
            if let Err(e) = guic_template(&tplt) {
                error!("guic error: {}", e.message);
                if let Some(e) = e.source() {
                    error!("Caused by: {}", e);
                }
                std::process::exit(1);
            }
        }
        Command::Crypt(crypt) => {
            if crypt.decrypt {
                match gdecrypt(&crypt.data, crypt.key.as_deref()) {
                    Ok(msg) => {
                        println!("Decrypted data : {}", msg);
                    }
                    Err(e) => println!("Error : {}", e.message),
                }
            } else {
                match gencrypt(&crypt.data, crypt.key.as_deref()) {
                    Ok(msg) => {
                        println!("Encrypted data : {}", msg);
                    }
                    Err(e) => println!("Error : {}", e.message),
                }
            }
        }
        Command::GenKey(opts) => {
            let l = opts.length.unwrap_or(256);
            let key = gen_key(l as usize);
            println!("Key: \n{}", key);
        }
    }

    println!("done.");
}

fn init_logs(log_level: usize) {
    let root_level_filter = match log_level {
        0 => LevelFilter::Error,
        1 => LevelFilter::Warn,
        2 => LevelFilter::Info,
        3 => LevelFilter::Debug,
        _ => LevelFilter::Trace,
    };
    SimpleLogger::new()
        .with_level(LevelFilter::Error)
        .with_module_level("guidon", root_level_filter)
        .init()
        .unwrap();
}

fn init_guidon(opts: &TemplateOpts) -> Result<Guidon> {
    if opts.git_source {
        let git_opts = opts.git_opts().unwrap();
        let mut builder = GitOptions::builder();
        builder.repo(opts.from_path.clone());
        builder.unsecure(git_opts.unsecure);
        builder.auto_proxy(git_opts.auto_proxy);
        if let Some(r) = git_opts.rev {
            builder.rev(r);
        }
        let git_options = builder.build().expect("Can't parse git options");

        Guidon::try_new(git_options)
    } else if opts.custom_tplt.is_none() {
        Guidon::try_new(&opts.from_path)
    } else {
        let template_path = Path::new(opts.custom_tplt.as_ref().unwrap());
        Guidon::try_new(CustomTemplate {dir_path: Path::new(&opts.from_path), template_path})
    }
}


fn render_callback(key: String) -> String {
    let mut data = String::new();
    print!(" - {} ? []: ", key);
    let _ = std::io::stdout().flush();
    let _ = std::io::stdin().read_line(&mut data);
    let mut r = data.split(':').next().unwrap();
    r = r.trim();
    if r.is_empty() {
        r = "";
    }
    r.to_string()
}

fn variables_callback(vars: &mut BTreeMap<String, String>) {
    for (key, value) in vars.iter_mut() {
        let mut data = String::new();
        print!(" - {} ? [{}]: ", key, value);
        let _ = std::io::stdout().flush();
        let _ = std::io::stdin().read_line(&mut data);
        let mut r = data.split(':').next().unwrap();
        r = r.trim();
        if !r.is_empty() {
            *value = r.to_string();
        }
    }
}

/// Apply template to project structures
///
///
///
#[derive(Options, Debug)]
pub(crate) struct CliOpts {
    #[options(help = "help")]
    pub help: bool,
    /// Verbose mode (-v: logs, -vv: more logs, -vvv: logs... and logs...)
    #[options(help = "verbosity", count)]
    pub verbose: usize,
    #[options(command)]
    pub command: Option<Command>,
}

#[derive(Debug, Options)]
enum Command {
    #[options(help = "Templatize the given data", name = "tplt")]
    Template(TemplateOpts),
    #[options(help = "Encrypt / Decrypt")]
    Crypt(CryptoOpts),
    #[options(help = "Generate key", name = "key")]
    GenKey(GenKeyOpts),
}

#[derive(Debug, Options)]
struct GenKeyOpts {
    #[options(help = "The length of the key. Should be 256", free)]
    pub length: Option<u16>,
}

#[derive(Debug, Options)]
struct CryptoOpts {
    #[options(
        help = "set to true to decrypt given data. If false, data encryption",
        short = "x"
    )]
    pub decrypt: bool,
    #[options(
        help = "The key to use. If not provided, the key will be read from GUIDON_KEY env var",
        short = "k"
    )]
    pub key: Option<String>,
    #[options(help = "The initialization vector to use")]
    pub iv: Option<String>,
    #[options(help = "The data to encrypt/decrypt", free)]
    pub data: String,
}

#[derive(Debug, Options)]
struct TemplateOpts {
    /// Git source : use this flag if the template is hosted in a git repo
    #[options(help = "Required if template is hosted in a git repo")]
    pub git_source: bool,
    /// git - Revision of the repository to use.
    ///
    /// Can be a tag (`1.0`), a branch (`origin/my_branch`) or a revision id.
    /// By default the value is set to `master`
    #[options(help = "Revision to use")]
    pub rev: Option<String>,
    /// git - If set to `true`, certificate validity is not checked
    #[options(help = "True to ignore certificate validation", short = "k")]
    pub unsecure: bool,
    /// git - If set to `true`, git tries to detect proxy configuration
    #[options(help = "If set to true, git tries to detect proxy configuration")]
    pub auto_proxy: bool,
    /// Path of the template directory
    ///
    /// If the git flag is specified, it's the URL of the git repo.
    /// Else it's the folder where the template can be found.
    /// If the path resolves to a file, guic assumes it's the config file,
    /// and that the its parent dir is the working dir.
    #[options(help = "Path or git repo of the template", free)]
    pub from_path: String,
    /// Output path for the target
    #[options(help = "Output path for the target", free)]
    pub to_path: String,
    /// Render mode.
    ///
    /// If `lax`, missing values are defaulted to an empty string,
    /// if `strict` an error is raised for a missing value,
    /// if `strict_ask`, user is prompted for missing values.
    #[options(help = "Render mode (lax, strict or strict_ask)", parse(try_from_str))]
    pub mode: Mode,
    /// Interacive : ask for variables values at runtime
    #[options(
        help = "Review variables before rendering template",
        long = "interactive",
        short = "i"
    )]
    pub interactive: bool,
    /// Wether the template files are located in a template folder or not.
    ///
    /// By default a template dir is used. Its name is always `template`.
    #[options(
        help = "If a custom template is used.",
        short = "t"
    )]
    pub custom_tplt: Option<String>,
}

impl TemplateOpts {
    fn git_opts(&self) -> Option<GitOpts> {
        if self.git_source {
            Some(GitOpts {
                rev: self.rev.as_ref(),
                unsecure: self.unsecure,
                auto_proxy: self.auto_proxy,
            })
        } else {
            None
        }
    }
}

#[derive(Debug)]
pub(crate) enum Mode {
    Lax,
    Strict,
    StrictAsk,
}

impl Default for Mode {
    fn default() -> Self {
        Mode::Strict
    }
}

impl FromStr for Mode {
    type Err = GuidonError;

    fn from_str(mode: &str) -> Result<Self> {
        match mode {
            "lax" => Ok(Mode::Lax),
            "strict" => Ok(Mode::Strict),
            "strict_ask" => Ok(Mode::StrictAsk),
            _ => Err(GuidonError::from_string(&format!(
                "Could not parse {}",
                mode
            ))),
        }
    }
}

#[derive(Debug)]
pub(crate) struct GitOpts<'a> {
    /// git - Revision of the repository to use.
    ///
    /// Can be a tag (`1.0`), a branch (`origin/my_branch`) or a revision id.
    /// By default the value is set to `master`
    pub rev: Option<&'a String>,
    /// git - If set to `true`, certificate validity is not checked
    pub unsecure: bool,
    /// git - If set to `true`, git tries to detect proxy configuration
    pub auto_proxy: bool,
}
